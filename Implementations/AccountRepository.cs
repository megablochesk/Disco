﻿using System;
using System.Collections.Generic;
using System.Linq;
using Disco.Data.Models;
using Disco.Repositories;

namespace Disco.Implementations
{
    public class AccountRepository : IAccountRepository
    {
        private static List<Account> accounts = new List<Account>();

        public AccountRepository()
        {
        }

        public void AddAccount(Account account)
        {
            accounts.Add(account);
            //saveChanges()
        }

        public Account GetAccountByUsername(string username)
        {
            return accounts.FirstOrDefault(a => a.Username == username);
        }
    }
}
