﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Disco.AuthConfig;
using Disco.Repositories;
using Disco.Utils;
using Disco.Data;

namespace Disco.Managers
{
    public class TokenManager
    {
        private readonly IAccountRepository _AccountRepository;

        public TokenManager(IAccountRepository accountRepository)
        {
            _AccountRepository = accountRepository;
        }

        public string GenerateToken(string username, string password)
        {
            var identity = GetIdentity(username, password);
            if (identity == null)
            {
                return string.Empty;
            }

            var now = DateTime.UtcNow;

            var jwt = new JwtSecurityToken(
                issuer: AuthOptions.ISSUER,
                audience: AuthOptions.AUDIENCE,
                notBefore: now,
                claims: identity.Claims,
                expires: now.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
                signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(),
                SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            return encodedJwt;
        }

        private ClaimsIdentity GetIdentity(string username, string password)
        {
            var account = _AccountRepository.GetAccountByUsername(username);
            if (account != null && SecurePasswordHasher.Verify(password, account.Password))
            {
                var role = Enum.GetName(typeof(RolesEnum), account.Role);

                var claims = new List<Claim>
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, account.Username),
                    new Claim(ClaimsIdentity.DefaultRoleClaimType, role),
                };

                ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims, "Token",
                    ClaimsIdentity.DefaultNameClaimType,
                    ClaimsIdentity.DefaultRoleClaimType);
                return claimsIdentity;
            }

            return null;
        }
    }
}
