﻿using System;
using Disco.Data;
using Disco.Data.DTO;
using Disco.Data.Models;
using Disco.Repositories;
using Disco.Utils;

namespace Disco.Managers
{
    public class AccountManager
    {
        private readonly IAccountRepository _AccountRepository;

        public AccountManager(IAccountRepository accountRepository)
        {
            _AccountRepository = accountRepository;
        }

        public (bool, string) CreateAccount(AccountDTO account)
        {
            var validationMessage = ValidateAccount(account);
            if(!string.IsNullOrEmpty(validationMessage))
            {
                return (false, validationMessage);
            }

            var accountToCreate = new Account
            {
                Username = account.Username,
                Password = SecurePasswordHasher.Hash(account.Password),
                Description = account.Description,
                Role = account.IsDJ ? RolesEnum.DJ : RolesEnum.User
            };

            _AccountRepository.AddAccount(accountToCreate);
            return (true, string.Empty);
        }

        private string ValidateAccount(AccountDTO account)
        {
            if(account.Password != account.PasswordConfirm)
            {
                return "Password confirm is not correct";
            }
            if(string.IsNullOrWhiteSpace(account.Password)
                || string.IsNullOrWhiteSpace(account.PasswordConfirm))
            {
                return "Password cannot de empty";
            }
            if (string.IsNullOrWhiteSpace(account.Username))
            {
                return "Username cannot de empty";
            }
            return string.Empty;
        }
    }
}
