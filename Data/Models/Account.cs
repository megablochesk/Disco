﻿using System;
namespace Disco.Data.Models
{
    public class Account
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Description { get; set; }
        public RolesEnum Role { get; set; }
    }
}
