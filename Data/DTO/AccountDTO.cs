﻿using System;

namespace Disco.Data.DTO
{
    public class AccountDTO
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string PasswordConfirm { get; set; }
        public string Description { get; set; }
        public bool IsDJ { get; set; } = false;
    }
}
