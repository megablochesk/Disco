﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Disco.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    [Authorize]
    public class PartyController : Controller
    {
        [HttpGet]
        public IEnumerable<string> GetParty()
        {
            return new string[] { "New Year Eve", "Birthday" };
        }
    }
}
