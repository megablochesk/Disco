﻿using Disco.Managers;
using Disco.Data.DTO;
using Microsoft.AspNetCore.Mvc;

namespace Disco.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class AccountController : Controller
    {
        private readonly TokenManager _TokenManager;
        private readonly AccountManager _AccountManager;

        public AccountController(TokenManager tokenManager, AccountManager accountManager)
        {
            _TokenManager = tokenManager;
            _AccountManager = accountManager;
        }

        [HttpGet]
        public IActionResult Token(string username, string password)
        {
            var token = _TokenManager.GenerateToken(username, password);
            if (string.IsNullOrEmpty(token))
            {
                return BadRequest(new { errorText = "Invalid username or password." });
            }

            var response = new
            {
                access_token = token,
                username = username
            };

            return Json(response);
        }

        [HttpPost]
        public IActionResult Register(AccountDTO account)
        {
            var (isSucceded, validationMessage) =
                _AccountManager.CreateAccount(account);

            if(isSucceded)
                return Ok();
            else
                return BadRequest(validationMessage);
        }
    }
}
