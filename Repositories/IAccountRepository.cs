﻿using System;
using Disco.Data.Models;

namespace Disco.Repositories
{
    public interface IAccountRepository
    {
        Account GetAccountByUsername(string username);
        void AddAccount(Account account);
    }
}
